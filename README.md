# Kubernetes



## Intro:

Kubernetes est une stack qui permet la gestion de différents conteneur. En effet, c’est un orchestrateur qui permet via des module (se sont eux même des pods) compris dans le Master Node, de gérer plusieurs Node contenant des pods et eux même contenant un ou plusieurs conteneurs. K8S permet d’exercer plusieur fonction comme le Load-Balancing , la Schedulation permettant la haute disponibilité des applications , et donc le maintient de leur fonctionnalité en tous temps.

![Ici une image illustrant les différentes structures](https://platform9.com/wp-content/uploads/2019/05/kubernetes-constructs-concepts-architecture-640x500.jpg)


### En résumer , au niveau du Master , il y a :

- ***ETCD*** : C’est une BDD qui stocke toute les donnée du K8S sous forme de clé valeur afin de les lister les donnés d’un nœud , d’un pod etc...
- ***Controller Manager***: Permet de cartographier les ressources disponible , les capacités physique etc. Pour faire fonctionner un Node. Si un Node   ne fonctionne plus , le controller Manager va envoyer ces info au Scheduler par l’intermédiaire du Kube API Server. Qui va ensuite faire le nécessaire pour palier au problème.
- ***Scheduler*** : Permet de Déployer une à plusieurs application en les répliquant à l’identique dans différent conteneur dans un Pod permettant ainsi le maintient de l’application en question et si un conteneur n’est plus fonctionnel ou qu’une info de dysfonctionnement est venu par le Controller Manager , le Scheduler va basculer sur un autre conteneur focntionnel. 
- ***Kube API Server*** : C’est le cerveau du Master Node, permet d’être en contacte entre les différent module et qui permet d’interpreter les commande qu’on envoie en les exécutant sur les différent module.  De plus il est en contacte avec les kubelet des Worker Nodes leur donnant ainsi des infos.

### Au niveau des Worker : 

Chaque Workers Nodes possèdent au moins un Pod , contenant eux même au moins un conteneur. De plus , chaque Worker Nodes execute chacun deux processus que sont le Kubelet et le Kube-proxy (qu'on appel aussi des daemons car toujour actif) : 

- ***Kubelet*** : C'est processus qui permet de recevoir les demandent provenant du Kube API Server, afin de les executer sur les conteneurs. Il permet aussi de gerer les ressources nécessaire. Kubelet interagie avec le 'Container Runtime Engine' qui sera ici Docker mais on peut utiliser d'autre Engine comme par exemple Rocket. 
- ***Kube-Proxy*** : Comme sont nom l'indique ce processus intéragie avec le réseau. En effet, il permet de créer et gèrer les règles pour exposer les conteneur sur le réseau. 

## Les différentes ressources : 

Pour la suite des infos sur Kubernetes , il existe une commande kubernetes pratique pour comprendre ***les alias*** des differentes commandes qui est :
	
	kubectl api-resources

qui vous donnera tous les alias disponible dans K8S. 

## **1. Namespace** : 

Mechanisme permettant d’isoler les groupes dans un seul cluster. Les noms des ressources doivent être unique au sein d’un espace Namespace,    mais pas entre les Namespaces. Ces derniers agissent sur les objets qui peuvent y être appliquer comme les deployment , les services etc. mais pas les objets à l’échelle du cluster comme le StorageClass ou les Nodes , les Volume etc. 
       
### Afficher les namespaces disponibles : 
       
	kubectl get ns
       
### Créé une namespace : 
       
    kubectl create ns <NOM DU NS> 


## **2. Pods** : 

C’est un groupe d’un ou plusieurs conteneur. Il contient donc un ou plusieurs même 	applications. On peut créé un conteneur dans un pods avec une image donné, puis modifier 	cette image là via un changement de la version de l’image dans un fichier YAML. 
Il existe différent types de pods : 
- ***Statique*** : ceux présents dans le dossier /etc/kubernetes/manifests , en fichier yaml. Ces pods ne peuvent être supprimer à part si on supprime le fichier dans se dossier là.
- ***Non statique*** : se sont les pods que l'on crée en ligne de commande (voir les commandes plus bas) et qui ne se présente pas dans le dossier manifests cité ci-dessus. 

### Donner la liste de pods : 
	kubectl get pods 
	
### Créé un pod contenant un conteneur avec une image nginx : 

	kubectl run <NOM DU PODS> --image nginx 

### Créé un pod sans le lancer afin d'avoir un fichier yaml du pod : 

	kubectl run <NOM DU PODS> --image nginx –dry-run=client -oyaml >  pods.yaml
	
### Créé un pod à partir d'un fichier yaml avec la commande suivant : 

	kubectl create -f pods.yaml

### Modifier un pods via son fichier : 

	kubectl apply -f <fichier yaml modifier> 
				ou 
	kubectl replace -f <fichier yaml modifier>

### Afficher la description d’un pod : 
					
	kubectl describe pods/<NOM DU POD>

### Créé un pod dans un ns : 
					
	kubectl run -n <NS> <NOM DU PODS> --image nginx 

### Afficher les pods présent dans le NS : 

	kubectl get pods -n <NS>

## **3. Deployments** :

Un deployments contient un pods et permet d’avoir plusieurs fonctionnalité comme effectuer des replicas par exemple ou les schedules ou encore les retour à la version précedente. Le mieux est de toujours faire un deployements pour ouvrir des pods.

### Créé un deployement avec des replicas : 
       
    kubectl create deploy <NOM DU DEPLOY> --image nginx –replicas=<nb> 
                          
### Afficher les deploy  dans des NS (se déclare comme pour les pods): 
				
    kubectl get deploy -n <NS> 


### Scale-up ou scale-down le deploy : 

	kubectl scale deploy <NOM DU DEPLOY> --replicas <nb> 

### Modifier l'image d'un pods via une commande : 

	kubectl set image deploy <NOM DU DEPLOY> image update=<image:version> 

### Voir l'historique de révision (modification de version) d'un deploy : 

	kubectl rollout history deploy <NOM DU DEPLOY> 

### Revenir à une version précedente ou suivante d'une révision : 

	kubectl undo deploy <NOM DU DEPLOY> --to-revision=<nb> 

## **4. ConfigMap** :

Permet de stocker les donné non confidentiels sous forme de clé , valeur. On peut utiliser les donner du configMap sous forme de variable dans les pods. 
Les donné du ConfigMap ne seront pas encrypter comme pour les Secrets. 
### Créé une ConfigMap via un fichier : 
	kubectl create configmap game-config-2 –from-file=<nom-du-fichier>
	
### Créé une ConfigMap avec des valeur brut : 

	kubectl create configmap  --from-literal=<clé1>=<valeur1> --from-literal <clé2>=<valeur2>
Ci-dessus les deux façons d'écrire (avec égale ou sans égale).
Pour les utiliser on va dans le fichier yaml du deployment et on peut les rajouter en tant que 	variable.

